import alglonrithm

if __name__ == '__main__':
    app = alglonrithm.create_app()
    app.run(host='0.0.0.0', port=8080, debug=True, threaded=True)