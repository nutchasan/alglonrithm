__version__ = '0.0.1'

from flask import Flask

from . import views
# from . import models
from . import default_settings
import alklornrithm


def create_app():
    app = Flask(__name__)
    app.config.from_object(default_settings)
    app.config.from_envvar('ALGLONRITHM_SETTINGS')
    # app.config.from_envvar('REDDOT_SETTINGS', silent=True)

    views.register_blueprint(app)
    
    return app


