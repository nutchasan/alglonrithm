from flask_mongoengine import MongoEngine
from .users import User
# from .otophistorylog import OtopHistory
# from .otopproduct import (OtopProduct, 
#                           FoodDetail, 
#                           OtopPackage,
#                           SelectedIngredient,
#                           Machine,
#                           OtopImage)
# from .otopentrepreneur import OtopEntrepreneur, OtopAddress
# from .ingredients import IngredientMain, IngredientSub
# from .otopfinance import OtopFinance, TransferDetail

 __all__ = [User]

db = MongoEngine()

def init_db(app):
    db.init_app(app)
