import mongoengine as me
import datetime

class Verse(me.EmbeddedDocment):
    content = me.ListField(StringField(max_length=100))

class User(me.Document):
    username = me.StringField(required=True, unique=True, max_length=20)
    versename = me.StringsField(required=True, uniqe=True, max_lenght=20)
    point =  me.IntField(required=True)
    rank = me.IntField(required=True)
    verse = me.EmbeddedDocumentField("Verse", required=True)

    # password = me.StringField(required=True)
    # email = me.StringField()
    # phone_num = me.StringField(required=True)

    # first_name = me.StringField(required=True)
    # last_name = me.StringField(required=True)
    # nick_name = me.StringField(required=True)
    # position = me.StringField()
    # active = me.BooleanField(default=False)
    # roles = me.ListField(me.StringField(), default=['user'])
    # created_date = me.DateTimeField(required=True,
    #                                 default=datetime.datetime.utcnow)
    # update_date = me.DateTimeField(required=True,
    #                                 default=datetime.datetime.utcnow)

    meta = {'collection': 'users'}
