from flask import (Blueprint, 
                   request, 
                   render_template, 
                   redirect, 
                   url_for,
                   current_app)

from alglonrithm.forms import(RegisterForm)

module = Blueprint('dashboard', __name__, static_url_path='/static')

@module.route('/', methods=['GET', 'POST'])
def dashboard():
    form = RegisterForm()

    if not form.validate_on_submit():
        return render_template('/dashboard.html',form=form)
    return redirect(url_for('edit_glon.edit_glon'))
    
    