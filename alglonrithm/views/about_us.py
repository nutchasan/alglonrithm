from flask import (Blueprint, 
                   request, 
                   render_template, 
                   redirect, 
                   url_for,
                   current_app)

module = Blueprint('about_us', __name__, static_url_path='/static')

@module.route('/about_us')
def about_us():
    return render_template('/about_us.html')