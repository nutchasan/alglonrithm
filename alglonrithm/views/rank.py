from flask import (Blueprint, 
                   request, 
                   render_template, 
                   redirect, 
                   url_for,
                   current_app)

module = Blueprint('rank', __name__, static_url_path='/static')

@module.route('/rank')
def rank():
    return render_template('/rank.html')