from flask import (Blueprint, 
                   request, 
                   render_template, 
                   redirect, 
                   url_for,
                   current_app)

from alglonrithm.forms import(VerseForm)
import alklornrithm


module = Blueprint('edit_glon', __name__, static_url_path='/static')

@module.route('/edit_glon', methods=['GET', 'POST'])
def edit_glon():
        form = VerseForm()
    
        if not form.validate_on_submit():
                return render_template('/edit_glon.html',form=form)
        if form.validate_on_submit():
                data1 = form.content1.data
                data2 = form.content2.data
                data3 = form.content3.data
                data4 = form.content4.data
                print(data1,data2,data3,data4)
                result = alklornrithm.alklorn(data1,
                                data2,
                                data3,
                                data4)
                return render_template('/scoreboard.html',result=result) 

# @module.route('/scoreboard', methods=['GET', 'POST'])
# def scoreboard(data1,data2,data3,data4):
#     return render_template('/scoreboard.html',result=result)        
   