from flask import Flask
from . import dashboard
from . import rank
from . import about_us
from . import edit_glon
# from . import scoreboard
# from . import admin
# from . import history
# from . import finances


def register_blueprint(app):
    app.register_blueprint(dashboard.module)
    app.register_blueprint(rank.module)
    app.register_blueprint(about_us.module)
    app.register_blueprint(edit_glon.module)
    # app.register_blueprint(scoreboard.module)
#     app.register_blueprint(admin.module)
#     app.register_blueprint(history.module)
#     app.register_blueprint(finances.module)

