from flask_wtf import FlaskForm
from wtforms import Form
from wtforms import fields
from wtforms.fields import html5
from wtforms import validators

class VerseForm(FlaskForm):
    content1 = fields.TextField('Fill you content', validators=[validators.InputRequired(),
                                validators.Length(min=3)])
    content2 = fields.TextField('Fill you content', validators=[validators.InputRequired(),
                                validators.Length(min=3)])
    content3 = fields.TextField('Fill you content', validators=[validators.InputRequired(),
                                validators.Length(min=3)])
    content4 = fields.TextField('Fill you content', validators=[validators.InputRequired(),
                                validators.Length(min=3)])                                                                                    