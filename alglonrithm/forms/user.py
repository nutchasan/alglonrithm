from flask_wtf import FlaskForm
from wtforms import Form
from wtforms import fields
from wtforms.fields import html5
from wtforms import validators

class RegisterForm(FlaskForm):
    username = fields.TextField('name', validators=[validators.InputRequired(),
                                validators.Length(min=3)])
    versename = fields.TextField('versename', validators=[validators.InputRequired(),
                                validators.Length(min=3)])

