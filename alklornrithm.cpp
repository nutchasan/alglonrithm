// #include<iostream>
// #include<string>
// #include<vector>

// using namespace std;

// int main(){
//     int sts;
//     string input;
//     string get_command;

//     cin >> input;

//     get_command.append("thpronun -p ");
//     get_command.append(input);
//     get_command.append(" > output.txt");

//     vector<char> cstr(get_command.c_str(), get_command.c_str() + get_command.size() + 1);


//     sts = system(command);

//     return 0;
// }
#include <cstdio>
#include <iostream>
#include <memory>
#include <stdexcept>
#include <string>
#include <array>
#include <vector>
#include <sstream>
#include <cstring>
#include <Python.h>

using namespace std;

const char vowels[] = {
    'a','e','i','o','u'
};
#define VOWELS_COUNT 5


vector<string> wuk_to_payang(string wuk){
    vector<string> payang;
    istringstream iss(wuk);
    string temp = "";

    while(iss >> temp){
        payang.push_back(temp);
    }
    return payang;
}

vector<vector<string>> text_to_payangs(const char* cmd) {
    int count=0;
    array<char, 128> buffer;
    vector<string> wuks;
    vector<vector<string>> result;
    unique_ptr<FILE, decltype(&pclose)> pipe(popen(cmd, "r"), pclose);
    if (!pipe) {
        throw runtime_error("popen() failed!");
    }
    while (fgets(buffer.data(), buffer.size(), pipe.get()) != nullptr) {
        if(count>0)
            wuks.push_back( buffer.data() );
        count++;
    }
    for(auto sub_result : wuks)
        result.push_back(wuk_to_payang(sub_result));

    return result;
}

bool is_rhyme(string payang1, string payang2){
    size_t ryhme1,ryhme2;


    for(int i=0; i<VOWELS_COUNT; i++){//size of vowels in thpronuns
        if(ryhme1 > payang1.find(vowels[i]))
            ryhme1 = payang1.find(vowels[i]);
        if(ryhme2 > payang2.find(vowels[i]))
            ryhme2 = payang2.find(vowels[i]);
    }
    string temp1 = payang1.substr(ryhme1,payang1.size() - (ryhme1+1) );
    string temp2 = payang2.substr(ryhme2,payang2.size() - (ryhme2+1) );

    // cout << temp1 << " and " << temp2 << endl;
    return (temp1.compare(temp2)==0)? true : false;
}

bool is_rhyme_in_alphabet(string payang1, string payang2){
    size_t ryhme1,ryhme2;

    ryhme1 = payang1.find(vowels[0]);
    ryhme2 = payang1.find(vowels[0]);
    for(int i=1; i<VOWELS_COUNT; i++){//size of vowels in thpronuns
        if(ryhme1 > payang1.find(vowels[i]))
            ryhme1 = payang1.find(vowels[i]);
        if(ryhme2 > payang2.find(vowels[i]))
            ryhme2 = payang2.find(vowels[i]);
    }
    string temp1 = payang1.substr(0,ryhme1-1);
    string temp2 = payang2.substr(0,ryhme2-1);

    return (temp1.compare(temp2)==0)? true : false;
}

int check_rhyme_between_wuk(vector<string> wuk1, vector<string> wuk2, int type_of_wuk){
    int result = 0;
    //0 means bad, 1 means good
    // cout << wuk1.at(wuk1.size()-1) << " vs " <<  wuk2.at(2) << "/" << wuk2.at(4) << endl;
    if(wuk1.size()==0 || wuk2.size()==0)return 0;


    switch(type_of_wuk){
        //สดับ-รับ
        case 1: //same as 3
        //รอง-ส่ง
        case 3: if( is_rhyme(wuk1.at(wuk1.size()-1), wuk2.at(2)) || is_rhyme(wuk1.at(wuk1.size()-1), wuk2.at(4)) )
                result = 1;
                break;
        //รับ-รอง
        case 2: //same as 4
        //ส่ง-ส่ง
        case 4: if( is_rhyme(wuk1.at(wuk1.size()-1), wuk2.at(wuk2.size()-1)) )
                result = 1;
                break;
        default: break;
    }
    

    return result;
}   

int check_rhyme_in_wuk(vector<string> wuk){
    int result = 0;
    // 0 means ok, 1 means good, 2 means very good
    if(wuk.size()==0)return 0;

    if( is_rhyme(wuk.at(2), wuk.at(3)) )
        result+=1;
    if( is_rhyme(wuk.at(4), wuk.at(5)) || is_rhyme(wuk.at(4), wuk.at(6)) )
        result+=1;
    
    return result;
}

int check_tone(vector<string> wuk, int type_of_wuk){
    if(wuk.size()==0)return 0;

    string check_wuk = wuk.at(wuk.size()-1);
    char tone = check_wuk.at(check_wuk.size()-1);
    int result = 0;
    // 0 means bad, 1 means ok, 2 means good
    
    switch(type_of_wuk){
        //สดับ
        case 1: if(tone == '0')
                    result = 1;
                else 
                    result = 2;
                break;
        //รับ
        case 2: if(tone == '0' || tone == '3')
                    result = 0;
                else if(tone == '4')
                    result = 2;
                else 
                    result = 1;
                break;
        //รอง
        case 3: // same as case 4
        //ส่ง
        case 4: if(tone == '0' || tone == '3')
                    result = 2;
                else 
                    result = 0;
                break;
        default: break;
    }

    return result;
}

int alklornrithm2(vector<vector<vector<string>>> klorn){


}

int alklornrithm( vector<vector<vector<string>>>klorn ){
    int result=0;
    int best_position[4];
    int temp;
    vector<vector<string>>::iterator it;
    vector<vector<string>> sadab;
    vector<vector<string>> rub;
    vector<vector<string>> rong;
    vector<vector<string>> song;

    for(int klorn_index=0; klorn_index<4; klorn_index++){
        it = klorn.at(klorn_index).begin();
        switch(klorn_index){
            case 0: sadab.assign(it,klorn.at(klorn_index).end());
                    break;
            case 1: rub.assign(it,klorn.at(klorn_index).end());
                    break;
            case 2: rong.assign(it,klorn.at(klorn_index).end());
                    break;
            case 3: song.assign(it,klorn.at(klorn_index).end());
                    break;
        }

        for(size_t i=0; i!=sadab.size(); ++i){
            for(size_t j=0; j!=rub.size(); ++j){
                for(size_t k=0; k!=rong.size(); ++k){
                    for(size_t l=0; l!=song.size(); ++l){
                        temp = check_rhyme_between_wuk(sadab.at(i),rub.at(j),1) +
                            check_rhyme_between_wuk(rub.at(j),rong.at(k), 2) +
                            check_rhyme_between_wuk(rong.at(k),song.at(l), 3) +
                            check_rhyme_in_wuk(sadab.at(i)) +
                            check_rhyme_in_wuk(rub.at(j)) +
                            check_rhyme_in_wuk(rong.at(k)) +
                            check_rhyme_in_wuk(song.at(l)) +
                            check_tone(sadab.at(i),1) +
                            check_tone(rub.at(j),2) +
                            check_tone(rong.at(k),3) +
                            check_tone(song.at(l),4);
                        if(sadab.at(i).size()!=8 || 
                        rub.at(j).size()!=8 ||
                        rong.at(k).size()!=8 ||
                        song.at(l).size()!=8
                            )
                            temp-=2;
                        if(result < temp){
                            result = temp;
                            best_position[0]=i;
                            best_position[1]=j;
                            best_position[2]=k;
                            best_position[3]=l;
                        }
                    }            
                }
            }
        }
        
    }

    // for(size_t i=0; i!=sadab.size(); ++i){
    //     for(size_t j=0; j!=rub.size(); ++j){
    //         for(size_t k=0; k!=rong.size(); ++k){
    //             for(size_t l=0; l!=song.size(); ++l){
    //                 temp = check_rhyme_between_wuk(sadab.at(i),rub.at(j),1) +
    //                        check_rhyme_between_wuk(rub.at(j),rong.at(k), 2) +
    //                        check_rhyme_between_wuk(rong.at(k),song.at(l), 3) +
    //                        check_rhyme_in_wuk(sadab.at(i)) +
    //                        check_rhyme_in_wuk(rub.at(j)) +
    //                        check_rhyme_in_wuk(rong.at(k)) +
    //                        check_rhyme_in_wuk(song.at(l)) +
    //                        check_tone(sadab.at(i),1) +
    //                        check_tone(rub.at(j),2) +
    //                        check_tone(rong.at(k),3) +
    //                        check_tone(song.at(l),4);
    //                 if(sadab.at(i).size()!=8 || 
    //                    rub.at(j).size()!=8 ||
    //                    rong.at(k).size()!=8 ||
    //                    song.at(l).size()!=8
    //                     )
    //                     temp-=2;
    //                 if(result < temp){
    //                     result = temp;
    //                     best_position[0]=i;
    //                     best_position[1]=j;
    //                     best_position[2]=k;
    //                     best_position[3]=l;
    //                 }
    //             }            
    //         }
    //     }
    // }

    cout << "best : ";
    for(int i=0;i<4;i++)
        cout << best_position[i] << "  ";
    cout << endl;
    cout << sadab.at(best_position[0]).size() << "&" << rub.at(best_position[1]).size() <<"&" << rong.at(best_position[2]).size() <<"&" << song.at(best_position[3]).size() << " : " ;
    cout << check_rhyme_between_wuk(sadab.at(best_position[0]),rub.at(best_position[1]),1);
    cout << check_rhyme_between_wuk(rub.at(best_position[1]),rong.at(best_position[2]), 2);
    cout << check_rhyme_between_wuk(rong.at(best_position[2]),song.at(best_position[3]), 3);
    cout << "/";
    cout << check_rhyme_in_wuk(sadab.at(best_position[0]));
    cout << check_rhyme_in_wuk(rub.at(best_position[1]));
    cout << check_rhyme_in_wuk(rong.at(best_position[2]));
    cout << check_rhyme_in_wuk(song.at(best_position[3]));
    cout << "/";
    cout << check_tone(sadab.at(best_position[0]),1);
    cout << check_tone(rub.at(best_position[1]),2);
    cout << check_tone(rong.at(best_position[2]),3);
    cout << check_tone(song.at(best_position[3]),4);
    cout << endl;
    for(auto sub_payang : sadab.at(best_position[0]))
        cout << sub_payang << " " ;
    cout << endl;
    for(auto sub_payang : rub.at(best_position[1]))
        cout << sub_payang << " " ;
    cout << endl;
    for(auto sub_payang : rong.at(best_position[2]))
        cout << sub_payang << " " ;        
    cout << endl;
    for(auto sub_payang : song.at(best_position[3]))
        cout << sub_payang << " " ;
    cout << endl;
    cout << "score = " << result  << endl;

    return result;
}


int call_alklorn(char* buffer[]){
    vector<vector<vector<string>>> klorn;
    int result;
    string command = "thpronun -p ";

    for(int i=0; i<4; i++){
        command.append(buffer[i]);
        klorn.push_back(text_to_payangs( command.c_str() ) );
        command.erase(command.begin()+12,command.end() );
    }

    return alklornrithm(klorn);
}

static PyObject *SpamError;

static PyObject *
alklorn(PyObject *self, PyObject *args){
    char* buffer[4];
    int result;

    if(!PyArg_ParseTuple(args, "ssss", &buffer[0], &buffer[1], &buffer[2], &buffer[3]))
        return NULL;
    result = call_alklorn(buffer);

    return Py_BuildValue("i", result);
}

static PyMethodDef alklornrithm_method[]={
    {"alklorn", alklorn, METH_VARARGS,
     "Execute a shell command."},
    {NULL, NULL , 0, NULL}//sentinel
};

static struct PyModuleDef alklornrithm_module{
    PyModuleDef_HEAD_INIT,
    "alklornrithm",//name of module
    NULL, // doc
    -1,
    alklornrithm_method
};

PyMODINIT_FUNC
PyInit_alklornrithm(void){
    return PyModule_Create(&alklornrithm_module);
}

// int main(
//     // string buffer[]
// ){
//     vector<vector<vector<string>>> klorn;
//     char* buffer[4] = { "สัมผัสในควรคงไว้บ้างเถิด",
//                         "เพิ่มไพเราะล้ำเลิศเกิดศักดิ์ศรี",
//                         "การใช้คำเลือกใช้อย่างกวี",
//                         "คำสูงต่ำเหมาะที่ไม่ปนเอย" };
//     int result;
//     string command = "thpronun -p ";

//     // if(argc < 2){
//     //     cout << "error : get more input";
//     //     exit(-1);
//     // } 


//     // for(int i=1; i<argc; i++){
//     //     klorn.push_back( text_to_payangs(buffer.append(argv[i]).c_str()) );

//     // }

//     for(int i=0; i<4; i++){
//         command.append(buffer[i]);
//         // cout << command << endl;
//         klorn.push_back(text_to_payangs( command.c_str() ) );
//         command.erase(command.begin()+12,command.end() );
//     }

//     // for(auto sub_buffer : klorn){
//     //     cout << sub_buffer.size() << endl;
//     //     for(auto ss : sub_buffer){
//     //         for(auto s : ss)
//     //             cout << s <<" " ;
//     //         cout << endl;
//     //     }
//     // }

//     // sadab = text_to_payangs("thpronun -p สัมผัสในควรคงไว้บ้างเถิด");
//     // rub = text_to_payangs("thpronun -p เพิ่มไพเราะล้ำเลิศเกิดศักดิ์ศรี");
//     // rong = text_to_payangs("thpronun -p การใช้คำเลือกใช้อย่างกวี");
//     // song = text_to_payangs("thpronun -p คำสูงต่ำเหมาะที่ไม่ปนเอย");

//     result = alklornrithm(klorn);  
      

//     // char get_klorn[500] = "thpronun -p ";
//     // if(argc > 1)
//     //     strcat(get_klorn,argv[1]);
//     // else {
//     //     cout << "error no input";
//     //     exit(-1);
//     // }

//     // wuks = text_to_wuk(get_klorn);    

//     // cout << "วรรคที่เป็นไปได้ :" << endl;
//     // for(auto sub_wuk : wuks){
//     //     cout << sub_wuk << endl;
//     // }
    
//     // for(auto sub_wuks : wuks){
//     //     payangs.push_back(wuk_to_payang(sub_wuks));
//     // }

//     // for(auto payang : payangs){
//     //     cout << "พยางค์ : ";
//     //     for(auto sub_payang : payang){
//     //         cout << sub_payang << " ";
//     //     }
//     //     cout << endl;
//     // }

//     return 0;   
// }

