from setuptools import setup, find_packages,Extension

module1 = Extension('alklornrithm',
                    sources = ['alklornrithm.cpp'])


f = open('requirement.txt', 'r')

requires = f.read().split('\n')

f.close()

setup(name='alglonrithm',
      version='0.0.1',
      description='',
      install_requires=requires,
      packages=find_packages(),
      include_package_data=True,
      author='',
      author_email='',
      url='',
      ext_modules = [module1]
     )
